<!-- HOME -->
<!-- TOP GALLERY -->
<div class="col-md-12 col-sm-12">
	<?php if (is_front_page()) { ?>
		<?php
		$images = get_field('home_gallery');
		if( $images ): ?>
		    <ul id="lightSlider">
		        <?php foreach( $images as $image ): ?>
		            <li>
		                <img class="img-responsive" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		            </li>
		        <?php endforeach; ?>
		    </ul>
		<?php endif; ?>
	<?php } ?>
	<!-- END OF GALLERY -->
</div>

<h3 class="info">
Since 1993, the consultants at Silverman Chapman &amp; Reese have offered sales strategy and direction to jewelry stores of every size. We've built our reputation on successfully restructuring jewellery businesses, selling off inventory, and liquidating entire stores. We apply proven strategies designed specifically for the jewellery business, whether you'd like higher revenues, retirement strategies, a jewellery business liquidation event.
</h3>
<!-- END OF CONTENT -->

<?php 
	$serviceShow = new WP_Query(
		array(
			'posts_per_page' => 4,
			'post_type' => 'service',
			'order' => 'ASC'
		)
	);
?>
<?php if ( $serviceShow->have_posts() ) : ?>
	<?php while ($serviceShow->have_posts()) : $serviceShow->the_post(); ?>
		<div class="col-md-6 col-xs-12 col-lg-3 service-home">
			<div class="thumbnail well">
				<?php $image = get_field('featured_image'); ?>
			    <img src="<?php echo $image['url'] ?>"/>
			    <div class="caption">
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p><?php the_field('short_description'); ?></p>
				</div>
			</div>
		</div>		
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
<?php else:  ?>
	<p>There are no case studies to be shown yet. We're working on this!</p>
<?php endif; ?>