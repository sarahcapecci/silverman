<?php
/*
Template Name: Main Template Page
*/
?>
<div class="main-content col-md-8 well">
	<h2 class="main-header"><?php the_title(); ?></h2>
	<!-- PARTICULAR TO ABOUT US PAGE - LEADERSHIP AND STAFF -->
	<?php if(is_page( 'about-us' )) { ?>	
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<h3>Leadership</h3>
		<?php while( has_sub_field('leadership') ): ?>
			<div class="about-people">
				<div class="col-md-4">
					<?php $leadership_img = get_sub_field('photo'); ?>
		    		<span class="profile thumbnail" style="background-image: url(<?php echo $leadership_img['url'] ?>);"></span>
		    	</div>
		    	<div class="col-md-8">
		    		<h4><?php echo get_sub_field('name'); ?></h4>
		    		<h5><?php echo get_sub_field('title'); ?></h5>
		    		<p><?php echo get_sub_field('bio'); ?></p>
		    	</div>
		    </div>
		<?php endwhile; ?>
		<h3>Staff</h3>
		<?php while( has_sub_field('staff') ): ?>
			<div class="about-people">
				<div class="col-md-4">
					<?php $staff_img = get_sub_field('photo'); ?>
		    		<span class="profile thumbnail" style="background-image: url(<?php echo $staff_img['url'] ?>);"></span>
		    	</div>
		    	<div class="col-md-8">
		    		<h4><?php echo get_sub_field('name'); ?></h4>
		    		<h5><?php echo get_sub_field('title'); ?></h5>
		    		<p><?php echo get_sub_field('bio'); ?></p>
		    	</div>
		    </div>
		<?php endwhile; ?>

	<!-- PARTICULAR TO HOME PAGE - SERVICES AT THE BOTTOM -->
	<?php } elseif (is_page( 'faq' )) { ?>
		<?php while( has_sub_field('q_and_a') ): ?>
			<div class="faq">
		    	<h4 class="question"><?php echo get_sub_field('question'); ?></h4>
		    	<p class="answer"><?php echo get_sub_field('answer'); ?></p>
		    </div>
		<?php endwhile; ?>
	<?php } elseif (is_page('contact-us')) { ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	<?php } ?>
</div>
<div class="second-sidebar col-md-3 well">
	<?php dynamic_sidebar('sidebar-footer'); ?>
</div>