<?php
/*
Template Name: Single Service Template
*/
?>
<div class="main-content col-md-8 well">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	  <?php $image = get_field('featured_image'); ?>
	  <img src="<?php echo $image['url']; ?>" alt="">
	  	<h2><?php the_title(); ?></h2>
	   <p class="roboto-normal font-md"><?php the_field('long_description'); ?></p>
	<?php endwhile; // end of the loop. ?>
</div>
<div class="second-sidebar col-md-3 well">
	<?php dynamic_sidebar('sidebar-footer'); ?>
</div>