<?php
/*
Template Name: Service Page
*/
$serviceShow = new WP_Query(
	array(
		'posts_per_page' => 4,
		'post_type' => 'service',
		'order' => 'ASC'
	)
);
?>
<div class="col-md-9 col-sm-12">
	<?php if ( $serviceShow->have_posts() ) : ?>
		<?php while ($serviceShow->have_posts()) : $serviceShow->the_post(); ?>
			<div class="col-md-6 col-sm-12">
				<div class="thumbnail well">
					<?php $image = get_field('featured_image'); ?>
				    <img src="<?php echo $image['url'] ?>"/>
				    <div class="caption">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<p><?php the_field('short_description'); ?></p>
					</div>
				</div>
			</div>		
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	<?php else:  ?>
		<p>There are no case studies to be shown yet. We're working on this!</p>
	<?php endif; ?>
</div>
<div class="second-sidebar col-md-3 col-sm-12 well">
	<?php dynamic_sidebar('sidebar-footer'); ?>
</div>