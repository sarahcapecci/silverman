<?php
/*
Template Name: Case Study Page
*/
$caseStudy = new WP_Query(
	array(
		'posts_per_page' => 50,
		'post_type' => 'case-study',
		'order' => 'ASC'
	)
);
?>
<div class="well">
	<h2 class="main-header"><?php the_title(); ?></h2>
	<?php if ( $caseStudy->have_posts() ) : ?>
		<?php while ($caseStudy->have_posts()) : $caseStudy->the_post(); ?>
			<div class="media clearfix">
			  <div class="col-md-3 col-sm-12">
			  	<?php $image = get_field('featured_image'); ?>
			    <img class="img-responsive media-object" src="<?php echo $image['url'] ?>" alt="Case Study Image">
			  </div>
			  <div class="col-md-9 col-sm-12">
			    <h3 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			    <p><?php the_field('short_description'); ?></p>
			  </div>
			</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	<?php else:  ?>
		<p>There are no case studies to be shown yet. We're working on this!</p>
	<?php endif; ?>
</div>