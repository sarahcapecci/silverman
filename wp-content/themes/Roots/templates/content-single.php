<?php while (have_posts()) : the_post(); ?>
  <?php 
    // check to see if the theme supports Featured Images, and one is set
        if (has_post_thumbnail( $post->ID )) {
                
            // specify desired image size in place of 'full'
            $page_bg_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
            $page_bg_image_url = $page_bg_image[0]; // this returns just the URL of the image

        } else {
            // the fallback – our current active theme's default bg image
            $page_bg_image_url = get_background_image();
        }
  ?>
  <article class="clearfix well">
  <div class="col-md-3 col-sm-12">
    <img src="<?php echo $page_bg_image_url ?>" alt="">
  </div>
  <div class="col-md-9 col-sm-12">
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  </div>
  </article>
<?php endwhile; ?>
