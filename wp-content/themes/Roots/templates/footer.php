<footer class="row" role="contentinfo">
	<div class="blog-feed">
		<?php
			$caseStudy = new WP_Query(
				array(
					'posts_per_page' => 3,
					'post_type' => 'case-study',
					'order' => 'ASC'
				)
			);

		?>
		<?php if ( $caseStudy->have_posts() ) : ?>
			<?php while ($caseStudy->have_posts()) : $caseStudy->the_post(); ?>
			<div class="media clearfix">
			  <div class="col-md-3 col-sm-3 col-xs-12">
			  	<?php $image = get_field('featured_image'); ?>
			    <a href="#">
			      <img class="img-responsive media-object" src="<?php echo $image['url'] ?>" alt="Case Study Image">
			    </a>
			  </div>
			  <div class="col-md-9 col-sm-9 col-xs-12">
			    <h3 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			    <p><?php the_field('short_description'); ?></p>
			  </div>
			</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="toll-num" style="clear:both"><a href="tel:18889551069">Toll Free - 1-888-955-1069</a></div>
	<div id="footer-brand"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logos/SCRC_Logo_web-white.png" alt="Silverman Chapman and Reese Consulting"></div>
	<span id="more-info"><a href="http://silvermanconsultants.com">Silverman Consultants US</a><a href="#">Staff Login</a></span>
	<p>Copyright &copy; <?php echo date('Y'); ?> Silverman Chapman and Reese Consulting. All rights reserved.</p>
</footer>
